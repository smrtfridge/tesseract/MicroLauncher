package cmd

import (
	"github.com/fatih/color"
	"github.com/spf13/cobra"
	"gitlab.com/tesseractge/MicroLauncher/log"
	log2 "log"
	"os"
)

// runCmd represents the run command
var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Run an application through MicroLauncher",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			log2.Fatal("[", color.RedString("FATAL"), "] No command specified")
			os.Exit(1)
		}
		log2.Println("Executing run", args[0])
		log.Log(log.INFO, "sassy gamers")
	},
}

func init() {
	rootCmd.AddCommand(runCmd)
	// saving: runCmd.PersistentFlags().String("command", "", "")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// runCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// runCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
