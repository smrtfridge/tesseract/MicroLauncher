package log

import "gitlab.com/tesseractge/MicroLauncher/log"

func main() {
	log.Log(log.OK, "This is an log with the level 'OK'")
	log.Log(log.INFO, "This is an log with the level 'INFO'")
	log.Log(log.WARN, "This is an log with the level 'WARN'")
	log.Log(log.FAILED, "This is an log with the level 'FAILED'")
	log.Log(log.FATAL, "This is an log with the level 'FATAL'")
	log.Log(log.EXTENSION, "This is an log with the level 'EXTENSION'")
	log.Log(log.DEBUG, "This is an log with the level 'DEBUG'")
}
