module gitlab.com/tesseractge/MicroLauncher

go 1.16

require (
	github.com/fatih/color v1.13.0
	github.com/spf13/cobra v1.3.0
	golang.org/x/sys v0.0.0-20220114195835-da31bd327af9 // indirect
)
